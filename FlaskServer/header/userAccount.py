import hashlib
import configparser
from sqlalchemy import create_engine, select, insert, MetaData, Table
from .baseDB import myDB

# for sha256 encryption
privateKey = b'proxmoxservice'


def comparePWD(userPwd: str, keyInPwd: str):
	# userPwd is encrypted, but keyInPwd is not
	encryptor = hashlib.sha256(privateKey)
	encryptor.update(keyInPwd.encode())
	keyInPwd_hash = encryptor.hexdigest()
	
	return userPwd == keyInPwd_hash


class userTB(myDB):
	def __init__(self, config, tableName = 'userAccount'):
		super().__init__(config)
		self.tableName = tableName

	def getUserPwd(self, userName: str):
		with self.engine.connect() as connect:
			stmt = select(self.table.c.password).where(self.table.c.email == userName)
			result = connect.execute(stmt)
			
		try:
			for row in result:
				password = row.password
			return password
		except:
			return None
		

	def createAccount(self, account, pwd, firstname, lastname, email):
		# store encrypt password
		encryptor = hashlib.sha256(privateKey)
		encryptor.update(pwd.encode('utf-8'))
		pwd_hash = encryptor.hexdigest()
		
		with self.engine.connect() as connect:
			stmt = (
				insert(self.table).
				values(
					account = account, password = pwd_hash, 
					firstName = firstname, lastName = lastname, 
					email = email
				)
			)
			
			connect.execute(stmt)

	def linkDB(self):
		try:
			self.engine = create_engine(self.connect_url)
		except Error as e:
			raise Warning("Create DB engine in failure...")
			
		metadata = MetaData()
		self.table = Table(self.tableName, metadata, autoload = True, autoload_with = self.engine)
			
	
	
