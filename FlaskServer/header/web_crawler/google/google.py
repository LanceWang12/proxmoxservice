from serpapi import GoogleSearch
import pandas as pd 
# import mysql.connector as connector
# import mysql
from sqlalchemy import create_engine

# get people_also_search_for
def get_google_df(name):
    params = {
    "q": name,
    "api_key": "5c69756a330986888802983c64b4305fcbface92cc888bc281f610b1d6c1ad6f"
    }
    search = GoogleSearch(params)
    results = search.get_dict()
    # results
    if 'knowledge' in results:
        knowledge_graph = results['knowledge_graph']
        # knowledge_graph
        print("has knowledge")
        key = 'people_also_search_for'
        if key in knowledge_graph.keys():
            print("exist")
            search = knowledge_graph[key]
            #print(type(search))
            #print(len(search))

            recommend =[]
            img = []
            key = []
            link = []
            for i in search:
                key.append(name)
                #print(i['name'])
                recommend.append(i['name'])
                #print(i['image'])
                img.append(i['image'])
                #print(i['link'])
                link.append(i['link'])

            dict = {'key': key, 'recommend': recommend, 'img': img, 'link':link }  
            df = pd.DataFrame(dict) 
            #print(df)
            return df
        else:
            print("noting search")
            return None
    else:
        print("noting search")
        return None


# get rank
def get_google_rank_df(name):
    params = {
    "q": name,
    "api_key": "5c69756a330986888802983c64b4305fcbface92cc888bc281f610b1d6c1ad6f"
    }
    search = GoogleSearch(params)
    results = search.get_dict()
    # results
    if 'knowledge_graph' in results:
        knowledge_graph = results['knowledge_graph']
        # knowledge_graph
        print("has knowledge")
        key = 'editorial_reviews'
        if key in knowledge_graph.keys():
            print("editorial_review")
            editorial_reviews = knowledge_graph['editorial_reviews']
            ranking = {}
            ranking['Rotten Tomatoes'] = 'no value'
            ranking['Metacritic'] = 'no value'
            ranking['IMDb'] = 'no value'
            ranking['Common Sense Media'] = 'no value'
            for i in editorial_reviews:
                print(i)
                if i['title'] == 'Rotten Tomatoes':
                    ranking['Rotten Tomatoes'] = i['rating']
                elif i['title'] == 'Metacritic' or i['title'] == 'Facebook':
                    ranking['Metacritic'] = i['rating']
                elif i['title'] == 'IMDb':
                    ranking['IMDb'] = i['rating']
                elif i['title'] == 'Common Sense Media':
                    ranking['Common Sense Media'] = i['rating']
                    
            # print(ranking)
            df = pd.DataFrame([ranking])
            return df
        else:
            return None
    else:
        return None

#print(get_google_search_df("Iron Man 3"))
#print(get_google_rank_df("Iron Man 3"))
