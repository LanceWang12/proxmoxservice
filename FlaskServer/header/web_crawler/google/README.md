# google knowledge grraph

| Column Name | Datatype     | PK   | Remarks                    |
| ----------- | ------------ | :--- | -------------------------- |
| key         | VARCHAR(60)  |      | movie_name                 |
| name        | VARCHAR(60)  |      | recommend movie            |
| img         | VARCHAR(400) |      | recommend movie img url    |
| link        | VARCHAR(500) |      | recommend movie search url |

