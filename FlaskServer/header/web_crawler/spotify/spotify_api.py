import requests
import pandas as pd

import spotipy
from spotipy.oauth2 import SpotifyClientCredentials


def spotify_api(query, num_albums = 10):
    
    # https://spotipy.readthedocs.io/en/2.19.0/#examples
    
    client_id = 'd3ecfd0057bd41a6a33e8cdd53cc1dbf'
    client_secret = '102ac4e55b594a229666cc498494f191'
    credential = SpotifyClientCredentials(client_id = client_id, client_secret = client_secret)
    sp = spotipy.Spotify(auth_manager = credential)
    
    # First the first album
    
    results = sp.search(q = query, limit = num_albums, type = 'album')
    album_list = []
    for index, album in enumerate(results['albums']['items']):
        album_list.append({'query': query, 'index': index + 1, 'album_name': album['name'], \
                           'album_url': album['external_urls']['spotify'], \
                           'image_path': album['images'][0]['url']})
    
    return {'results': album_list, 'num_albums': num_albums}


def get_spotify_df(query):
    
    json_data = spotify_api(query)
    
    if len(json_data['results']) == 0:
        return None
    
    df = pd.DataFrame(data = json_data['results'], columns = json_data['results'][0].keys())
    
    return df


if __name__ == '__main__':
    
    df = get_spotify_df(query = 'Richard III: The King in the Car Park')