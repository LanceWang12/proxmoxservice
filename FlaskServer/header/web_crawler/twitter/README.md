# twitter
| Column Name    | Datatype      | PK | Remarks           |
|----------------|---------------|----|-------------------|
| query          | VARCHAR(45)   | V  |                   |
| index          | INT(11)       | V  |                   |
| result_type    | VARCHAR(45)   |    | recent or popular |
| text           | VARCHAR(1024) |    | content of tweets |
| created_at     | DATETIME      |    | date and time     |
| favorite_count | INT(11)       |    | #likes            |
| lang           | VARCHAR(45)   |    | en or zh          |
| retweet_count  | INT(11)       |    | #retweets         |
