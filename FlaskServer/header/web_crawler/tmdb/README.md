# tmdb_1
| Column Name | Datatype    | PK | Remarks                                                                    |
|-------------|-------------|----|----------------------------------------------------------------------------|
| query       | VARCHAR(45) | V  | Things you want to search                                                  |
| index       | INT(11)     | V  |                                                                            |
| title       | VARCHAR(45) |    | movie's title                                                        |
| poster_path | VARCHAR(45) |    | movie's picture (Add `themoviedb.org/t/p/original` at the beginning) |

# tmdb_2
| Column Name       | Datatype      | PK | Remarks           |
|-------------------|---------------|----|-------------------|
| title             | VARCHAR(45)   | V  |                   |
| media_type        | VARCHAR(45)   |    | movie or tv       |
| original_language | VARCHAR(45)   |    | ex: en            |
| overview          | VARCHAR(1024) |    |                   |
| popularity        | DOUBLE        |    | no idea           |
| poster_path       | VARCHAR(45)   |    |                   |
| backdrop_path     | VARCHAR(45)   |    | background poster |
| vote_average      | DOUBLE        |    | score (1 ~ 10)    |
| vote_count        | INT(11)       |    |                   |
