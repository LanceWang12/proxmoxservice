import requests
import pandas as pd


api_key = 'd3f8f46fb1708d5ca438594644d5b461'

def tmdb_api_search(title, mode = 'multi'):
    
    assert mode in ['movie', 'tv', 'multi']
    url = f'https://api.themoviedb.org/3/search/{mode}?api_key={api_key}&query={title}'
    
    return requests.get(url).json()


def tmdb_api_get_cast(media_type, content_id):
    
    assert media_type in ['movie', 'tv']
    url = f'https://api.themoviedb.org/3/{media_type}/{content_id}/credits?api_key={api_key}'
    
    data = requests.get(url).json()
    
    # Cast
    cast_name_list = []
    for people in data['cast'][:10]:
        cast_name_list.append(people['name'])
    
    # Director
    director_name_list = []
    for people in data['crew']:
        if people['department'] == 'Directing' and people['job'] == 'Director':
            director_name_list.append(people['name'])
    
    return cast_name_list, director_name_list


def get_tmdb_1_df(query, top_k = 8):
    
    json_data = tmdb_api_search(query)
    
    cols = ['query', 'index', 'title', 'poster_path']
    data_list = []
    for index, data in enumerate(json_data['results']):
        
        if data.get('gender', None) is not None:
            # This is a person's info, just skip it
            continue
        
        data_list.append({'query': query, 'index': index + 1, \
                          'title': data['title'] if data['media_type'] == 'movie' else data['name'], \
                          'poster_path': data['poster_path']})
        
    df = pd.DataFrame(data = data_list[: top_k], columns = cols)
    
    return df


def get_tmdb_2_df(title):
    
    data = tmdb_api_search(title)['results'][0]
    
    # Create df
    cols = ['title', 'media_type', 'original_language', 'overview', 'popularity', \
            'poster_path', 'backdrop_path', 'vote_average', 'vote_count']
    row_dict = {'title': title}
    for col in cols:
        if col == 'title':
            continue
        row_dict[col] = [ data[col] ]
    df = pd.DataFrame(data = row_dict, columns = cols)
    
    # Get cast and directors (add to df)
    cast_name_list, director_name_list = tmdb_api_get_cast(data['media_type'], data['id'])
    df['cast'] = ', '.join(cast_name_list)
    df['director'] = ', '.join(director_name_list)
    
    return df


if __name__ == '__main__':
    
    # result = tmdb_api_search(title = 'big bang')
    # df = get_tmdb_1_df(query = 'avengers')
    # df = get_tmdb_1_df(query = 'haha')
    df = get_tmdb_2_df(title = 'Avengers: Endgame')
    # df = get_tmdb_2_df(title = 'The Big Bang Theory')
