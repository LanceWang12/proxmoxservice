import requests
import pandas as pd
from sqlalchemy import select, MetaData, Table, desc


from ..baseDB import myDB

from header.web_crawler.tmdb import get_tmdb_1_df, get_tmdb_2_df
from header.web_crawler.google import get_google_df
from header.web_crawler.spotify import get_spotify_df
from header.web_crawler.twitter import get_twitter_df
from header.web_crawler.youtube import get_review_df, get_trailer_df
from header.web_crawler.animation import get_animate_df

class web_crawler(myDB):
	def __init__(self, config):
		super().__init__(config)
		
		self.table_name = {
			'tm1': 'tmdb_1',
			'tm2': 'tmdb_2',
			'goo': 'google',
			'rev': 'review',
			'tra': 'trailer',
			'ani': 'animation',
			'spo': 'spotify',
			'twi': 'twitter'
		}
	
	def get_frontpage(self):
		self.linkDB()

		with self.engine.connect() as connect:
				stmt = select(self.tm2_table).order_by(desc(self.tm2_table.c.popularity)).limit(16)
				result = connect.execute(stmt)
			
		try:
			tmdb_df = self.sql2df(result)
		except:
			tmdb_df = None

		dict_lst = []
		for i in range(tmdb_df.shape[0]):
			query = tmdb_df['title'][i]
			
			# get params_dict
			twitter_df, spotify_df, review_df, trailer_df, google_df = self.get_data_from_db(query)
			params_dict = self.df2params_dict(tmdb_df, twitter_df, spotify_df, review_df, trailer_df, google_df)
			
			# get poster and title
			df = get_tmdb_1_df(query = query, top_k = 1)
			params_dict['title'] = df['query'][0]
			
			prefix = 'https://themoviedb.org/t/p/original'
			val = f"{prefix}{df['poster_path'][0]}"
			params_dict['poster'] = val

			dict_lst.append(params_dict)
			
		self.closeDB()
		
		return dict_lst

	def search_flow(self, query):
		df = get_tmdb_1_df(query = query)
		
		dict_lst = []
		self.linkDB()

		for i in range(df.shape[0]):
			print(f'<{i}> ')
			# -------- searching DB---------
			# search info in DB
			choose_result = df.iloc[i]['title']
			
			with self.engine.connect() as connect:
				stmt = select(self.tm2_table).where(self.tm2_table.c.title == choose_result)
				result = connect.execute(stmt)
			
			try:
				tmdb_df = self.sql2df(result)
				exist_title = tmdb_df['title'][0]
			except:
				exist_title = None
			
			
			if exist_title is None:
				print('-------- Not Exist Title --------')
				# if user's query is not in DB, we will insert to our DB
				tmdb_df = get_tmdb_2_df(title = choose_result)
				twitter_df = get_twitter_df(query = choose_result, lang = 'en', result_type = 'popular')
				spotify_df = get_spotify_df(query = choose_result)
				review_df = get_review_df(name = choose_result)
				trailer_df = get_trailer_df(name = choose_result)
				google_df = get_google_df(name = choose_result)
				#anime_df = get_animate_df(name = choose_result)
				print('Searching complete................')
			
				# save to DB
				if tmdb_df is not None:
					self.df2sql(tmdb_df, self.table_name['tm2'])
				if twitter_df is not None:
					self.df2sql(twitter_df, self.table_name['twi'])
				if spotify_df is not None:
					self.df2sql(spotify_df, self.table_name['spo'])
				if review_df is not None:
					self.df2sql(review_df, self.table_name['rev'])
				if trailer_df is not None:
					self.df2sql(trailer_df, self.table_name['tra'])
				if google_df is not None:
					self.df2sql(google_df, self.table_name['goo'])
			
				#if anime_df is not None:
					#self.df2sql(anime_df, self.table_name['ani'])
				
				print('Storing to DB complete................')
			else:
				print('-------- Exist Title --------')
				# if user's query is in our DB, we will select the info from our DB
				twitter_df, spotify_df, review_df, trailer_df, google_df = self.get_data_from_db(exist_title)
					
				#print(tmdb_df.shape, twitter_df.shape, spotify_df.shape, review_df.shape, trailer_df.shape, google_df.shape, anime_df)
			
			params_dict = self.df2params_dict(tmdb_df, twitter_df, spotify_df, review_df, trailer_df, google_df)
			
			# get title and poster
			params_dict['title'] = choose_result
			
			prefix = 'https://themoviedb.org/t/p/original'
			val = f"{prefix}{df.iloc[i]['poster_path']}"
			params_dict['poster'] = val
   
			dict_lst.append(params_dict)
		
		self.closeDB()
  
		return dict_lst

	def get_data_from_db(self, query):
		metadata = MetaData()
				
		twitter_table = Table(
			self.table_name['twi'], metadata, 
			autoload = True, autoload_with = self.engine
		)
		
		spotify_table = Table(
			self.table_name['spo'], metadata, 
			autoload = True, autoload_with = self.engine
		)
		
		review_table = Table(
			self.table_name['rev'], metadata, 
			autoload = True, autoload_with = self.engine
		)
		
		trailer_table = Table(
			self.table_name['tra'], metadata, 
			autoload = True, autoload_with = self.engine
		)
		
		google_table = Table(
			self.table_name['goo'], metadata, 
			autoload = True, autoload_with = self.engine
		)
		"""
		anime_table = Table(
			self.table_name['ani'], metadata, 
			autoload = True, autoload_with = self.engine
		)
		"""
		
		with self.engine.connect() as connect:
			# twitter
			stmt = select(twitter_table.c.favorite_count, twitter_table.c.retweet_count).where(
				twitter_table.c.query == query
			)
			result = connect.execute(stmt)
			twitter_df = self.sql2df(result)
			
			# spotify
			stmt = select(spotify_table.c.album_name, spotify_table.c.album_url, spotify_table.c.image_path).where(
				spotify_table.c.query == query
			)
			result = connect.execute(stmt)
			spotify_df = self.sql2df(result)
			
			# review
			stmt = select(review_table).where(
				review_table.c.name == query
			)
			result = connect.execute(stmt)
			review_df = self.sql2df(result)
			
			# trailer
			stmt = select(trailer_table).where(
				trailer_table.c.name == query
			)
			result = connect.execute(stmt)
			trailer_df = self.sql2df(result)
			
			# related recommendation
			stmt = select(google_table).where(
				google_table.c.key == query
			)
			result = connect.execute(stmt)
			google_df = self.sql2df(result)
			
			# anime
			"""
			stmt = select(anime_table).where(
				anime_table.c.name == query
			)
			result = connect.execute(stmt)
			anime_df = self.sql2df(result)
			"""
			
		#print(tmdb_df.shape, twitter_df.shape, spotify_df.shape, review_df.shape, trailer_df.shape, google_df.shape, anime_df)
		return twitter_df, spotify_df, review_df, trailer_df, google_df
	
	def df2params_dict(self, tmdb_df, twitter_df, spotify_df, review_df, trailer_df, google_df):
		params_dict = dict()

		# get favorite and retweet from twitter
		if twitter_df is not None:
			params_dict['favorite'] = round(twitter_df['favorite_count'].mean())
			params_dict['retweet'] = round(twitter_df['retweet_count'].mean())
		else:
			params_dict['favorite'] = None
			params_dict['retweet'] = None

		# get introduction, director, cast, tmdb_score
		if tmdb_df is not None:
			params_dict['intro'] = tmdb_df['overview'][0]
			params_dict['director'] = tmdb_df['director'][0]
			params_dict['cast'] = tmdb_df['cast'][0]
			params_dict['tmdb_score'] = round(tmdb_df['vote_average'][0], 1)
		else:
			params_dict['intro'] = None
			params_dict['director'] = None
			params_dict['cast'] = None
			params_dict['tmdb_score'] = None
			
		# get title, img, reference about album
		if spotify_df is not None:
			title_lst = []
			img_lst = []
			ref_lst = []
			for j in range(spotify_df.shape[0]):
				title_lst.append(spotify_df['album_name'][j])
				img_lst.append(spotify_df['image_path'][j])
				ref_lst.append(spotify_df['album_url'][j])

			params_dict['album_title'] = title_lst
			params_dict['album_img'] = img_lst
			params_dict['album_ref'] = ref_lst
		else:
			params_dict['album_title'] = None
			params_dict['album_img'] = None
			params_dict['album_ref'] = None
		
		# get review
		if review_df is not None:
			ref_lst = []
			for j in range(1, 7):
				val = f'https://www.youtube.com/embed/{review_df.iloc[0][j]}'
				ref_lst.append(val)
			params_dict['review'] = ref_lst
		else:
			params_dict['review'] = None
		
		# get trailer
		if trailer_df is not None:
			ref_lst = []
			for j in range(1, 6):
				val = f'https://www.youtube.com/embed/{trailer_df.iloc[0][j]}'
				ref_lst.append(val)
			params_dict['trailer'] = ref_lst
		else:
			params_dict['trailer'] = None
		
		# get related recommendation
		if google_df is not None:
			title_lst = []
			img_lst = []
			ref_lst = []
			for j in range(google_df.shape[0]):
				title_lst.append(google_df['recommend'][j])
				img_lst.append(google_df['img'][j])
				ref_lst.append(google_df['link'][j])
			
			params_dict['related_title'] = title_lst
			params_dict['related_img'] = img_lst
			params_dict['related_ref'] = ref_lst
		else:
			params_dict['related_title'] = None
			params_dict['related_img'] = None
			params_dict['related_ref'] = None
		
		
		return params_dict
     
 
	def sql2df(self, result):
		tmp = result.fetchall()
		
		if tmp == []:
			return None
		
		df = pd.DataFrame(tmp)
		df.columns = result.keys()
		
		return df
	
	def df2sql(self, df, tableName):
		with self.engine.connect() as connection:
			df.to_sql(tableName, connection, if_exists = 'append', index = False)
	
	def linkDB(self):
		super().linkDB()
		
		metadata = MetaData()
		self.tm2_table = Table(
			self.table_name['tm2'], metadata, 
			autoload = True, autoload_with = self.engine
		)
		
		
		
		
		
		
