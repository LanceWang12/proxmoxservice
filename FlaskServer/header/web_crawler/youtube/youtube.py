import requests
import json
from flask import Flask, request
import re
import urllib.request
import pandas as pd
# import mysql.connector as connector
# import mysql
from sqlalchemy import create_engine
from urllib.parse import quote

#get top 10 review
def get_review(name):
    name = str(name).split(" ")
    #print(name)
    query = '+'.join(str(quote(x)) for x in name)
    #print(query)
    # query = query.encode('utf-8')
    #print(type(query))
    url = "https://www.youtube.com/results?search_query=" + query + "+%E5%BD%B1%E8%A9%95"
    #print(url)
    html = urllib.request.urlopen(url)
    video_ids = re.findall(r"watch\?v=(\S{11})", html.read().decode())
    #print(len(video_ids))
    # relative
    if(len(video_ids)<10):
        #print('not-enough')
        num = 10-len(video_ids)
        for i in range(num):
            video_ids.append("no value")
    # get top 10 video
    video_ids = video_ids[:10]
    return video_ids

#trailer top 5 from youtube
def get_trailer(name):
    name = str(name).split(" ")
    # print(name)
    query = '+'.join(str(quote(x)) for x in name)
    # query = query.encode('utf-8')
    # print(type(query))
    #print(query)
    url = ("https://www.youtube.com/results?search_query=" + query + "trailer")
    #print(url)
    html = urllib.request.urlopen(url)
    video_ids = re.findall(r"watch\?v=(\S{11})", html.read().decode())
    # relative
    if(len(video_ids)<5):
        num = 5-len(video_ids)
        for i in range(num):
            video_ids.append("no value")
    # get top 5 video
    video_ids = video_ids[:5]
    return video_ids
    

def get_review_df(name):
    df = []
    id = get_review(name)
    #print(id)
    id.insert(0, name)
    #print(id)
    #convert to dataframe
    df = pd.DataFrame([id], columns = ['name', 'review1', 'review2','review3','review4','review5', 'review6', 'review7','review8','review9','review10'])
    #print(df)
    return df

def get_trailer_df(name):
    df = []
    id = get_trailer(name)
    #print(id)
    id.insert(0, name)
    #print(id)
    #convert to dataframe
    df = pd.DataFrame([id], columns = ['name', 'trailer1', 'trailer2','trailer3','trailer4','trailer5'])
    #print(df)
    return df


#get_review_df("brooklyn99")
#get_trailer_df("brooklyn99")
