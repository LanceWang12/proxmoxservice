import configparser

from header.web_crawler import web_crawler


if __name__ == '__main__':
	#target = 'spider man'
	target = 'Transformers'
	
	
	config = configparser.ConfigParser()
	config.read('./header/web_crawler/Crawler.ini')
	
	crawler = web_crawler(config)
	crawler.search_flow(target)
