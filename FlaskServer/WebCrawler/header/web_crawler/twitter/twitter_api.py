import requests
import tweepy
import pandas as pd


def twitter_api(query, lang = 'en', result_type = 'popular', num_tweets = 20):
    
    assert result_type in ['mixed', 'recent', 'popular']
    
    access_token = '2279411160-GCJYWOqX6UnYaYrsM4XEw5zXHfYcH7LnHF2XOHm'
    access_token_secret = 'xqzGrKJ4chsv122nF1VphirOjH0uYrdNmQXmSZJqcgL2u'
    api_key = 'z3tPcC14gDNkG8KsINPdogW26'
    api_key_secret = 'cOLkjrtaMA12nignlZ9k9Jvqkb5E9fwcZhQxir8jK4HpPaIwM1'
    
    auth = tweepy.OAuthHandler(consumer_key = api_key, consumer_secret = api_key_secret)
    auth.set_access_token(access_token, access_token_secret)
    api = tweepy.API(auth)
    
    data_list = []
    cols = ['query', 'index', 'text', 'created_at', 'favorite_count', 'lang', 'retweet_count', 'result_type']
    for index, tweet in enumerate(tweepy.Cursor(api.search_tweets, q = query, lang = lang, result_type = result_type).items(num_tweets)):
        data_dict = {'query': query, 'index': index + 1, 'result_type': tweet.metadata['result_type']}
        for col in cols:
            if col in ['query', 'index', 'result_type']:
                continue
            data_dict[col] = getattr(tweet, col)
        
        data_list.append(data_dict)
        
    return {'results': data_list, 'num_tweets': num_tweets}


def get_twitter_df(query, lang = 'en', result_type = 'popular'):
    
    json_data = twitter_api(query, lang, result_type)
    df = pd.DataFrame(data = json_data['results'], columns = json_data['results'][0].keys())
    
    return df


def test_twitter_api():
    
    params = {
        'query': 'spiderman',
        'num_tweets': 10,
        'lang': 'en'
    }
    result = twitter_api(**params)
    
    assert result['results'][0]['query'] == params['query']


if __name__ == '__main__':
    
    # app.run(host = '0.0.0.0', port = 5000)
    # app.run()
    
    # result = twitter_api(query = 'spiderman', lang = 'en', result_type = 'mixed')
    # result = twitter_api(query = '蜘蛛人', lang = 'zh')
    # test_twitter_api()
    
    df = get_twitter_df(query = 'spiderman', lang = 'en', result_type = 'mixed')
