# spotify
| Column Name | Datatype     | PK | Remarks                                                            |
|-------------|--------------|----|--------------------------------------------------------------------|
| query       | VARCHAR(45)  | V  |                                                                    |
| index       | INT(11)      | V  |                                                                    |
| album_name  | VARCHAR(256) |    |                                                                    |
| album_url   | VARCHAR(256) |    | you can use this url to open this album in the browser with spotify |
| image_path  | VARCHAR(256) |    |                                                                    |
