import requests
import pandas as pd


def tmdb_api(title, mode = 'multi', api_key = 'd3f8f46fb1708d5ca438594644d5b461', \
             print_url = False):
    
    assert mode in ['movie', 'tv', 'multi']
    url = f'https://api.themoviedb.org/3/search/{mode}?api_key={api_key}&query={title}'
    if print_url:
        print(url)
    
    return requests.get(url).json()


def get_tmdb_1_df(query):
    
    json_data = tmdb_api(query)
    
    cols = ['query', 'index', 'title', 'poster_path']
    data_list = []
    for index, data in enumerate(json_data['results']):
        
        data_list.append({'query': query, 'index': index + 1, \
                          'title': data['title'] if data['media_type'] == 'movie' else data['name'], \
                          'poster_path': data['poster_path']})
        
    df = pd.DataFrame(data = data_list[:10], columns = cols)
    
    return df


def get_tmdb_2_df(title):
    
    data = tmdb_api(title)['results'][0]
    
    cols = ['title', 'media_type', 'original_language', 'overview', 'popularity', \
            'poster_path', 'backdrop_path', 'vote_average', 'vote_count']
    row_dict = {'title': title}
    for col in cols:
        if col == 'title':
            continue
        row_dict[col] = [ data[col] ]
        
    df = pd.DataFrame(data = row_dict, columns = cols)
    
    return df



