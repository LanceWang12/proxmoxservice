import requests
import pandas as pd
from sqlalchemy import create_engine, select, insert, MetaData, Table

from ..baseDB import myDB

from header.web_crawler.tmdb import get_tmdb_1_df, get_tmdb_2_df
from header.web_crawler.google import get_google_df
from header.web_crawler.spotify import get_spotify_df
from header.web_crawler.twitter import get_twitter_df
from header.web_crawler.youtube import get_review_df, get_trailer_df
from header.web_crawler.animation import get_animate_df


class web_crawler(myDB):
	def __init__(self, config):
		super().__init__(config)
		
		self.table_name = {
			'tm1': 'tmdb_1',
			'tm2': 'tmdb_2',
			'goo': 'google',
			'rev': 'review',
			'tra': 'trailer',
			'ani': 'animation',
			'spo': 'spotify',
			'twi': 'twitter'
		}
		
	def search_flow(self, query):
		search_result = get_tmdb_1_df(query = query)
		
		# -------- user choose --------
		choose_result = self.user_choose(search_result)
		print(f'User chooses << {choose_result} >>...')
		
		# -------- other searching ---------
		self.linkDB()
		# search info in DB
		with self.engine.connect() as connect:
			stmt = select(self.tm2_table).where(self.tm2_table.c.title == choose_result)
			result = connect.execute(stmt)
		
		try:
			tmdb_df = self.sql2df(result)
			exist_title = tmdb_df['title'][0]
		except:
			exist_title = None
		
		#print(exist_title)
		
		if exist_title is None:
			# if user's query is not in DB, we will insert to our DB
			tmdb_df = get_tmdb_2_df(title = choose_result)
			twitter_df = get_twitter_df(query = choose_result, lang = 'en', result_type = 'mixed')
			spotify_df = get_spotify_df(query = choose_result)
			review_df = get_review_df(name = choose_result)
			trailer_df = get_trailer_df(name = choose_result)
			google_df = get_google_df(name = choose_result)
			anime_df = get_animate_df(name = choose_result)
			print('Searching complete........')
		
			# save to DB
			self.df2sql(tmdb, self.table_name['tm2'])
			self.df2sql(twitter, self.table_name['twi'])
			self.df2sql(spotify, self.table_name['spo'])
			self.df2sql(review, self.table_name['rev'])
			self.df2sql(trailer, self.table_name['tra'])
			self.df2sql(google, self.table_name['goo'])
		
			if anime_df is not None:
				self.df2sql(anime, self.table_name['ani'])
			
			print('Storing to DB complete........')
		else:
			# if user's query is in our DB, we will select the info from our DB
			metadata = MetaData()
			
			twitter_table = Table(
				self.table_name['twi'], metadata, 
				autoload = True, autoload_with = self.engine
			)
			
			spotify_table = Table(
				self.table_name['spo'], metadata, 
				autoload = True, autoload_with = self.engine
			)
			
			review_table = Table(
				self.table_name['rev'], metadata, 
				autoload = True, autoload_with = self.engine
			)
			
			trailer_table = Table(
				self.table_name['tra'], metadata, 
				autoload = True, autoload_with = self.engine
			)
			
			google_table = Table(
				self.table_name['goo'], metadata, 
				autoload = True, autoload_with = self.engine
			)
			
			anime_table = Table(
				self.table_name['ani'], metadata, 
				autoload = True, autoload_with = self.engine
			)
			
			with self.engine.connect() as connect:
				# twitter
				stmt = select(twitter_table).where(
					twitter_table.c.query == exist_title
				)
				result = connect.execute(stmt)
				twitter_df = self.sql2df(result)
				
				# spotify
				stmt = select(spotify_table).where(
					spotify_table.c.query == exist_title
				)
				result = connect.execute(stmt)
				spotify_df = self.sql2df(result)
				
				# review
				stmt = select(review_table).where(
					review_table.c.name == exist_title
				)
				result = connect.execute(stmt)
				review_df = self.sql2df(result)
				
				# trailer
				stmt = select(trailer_table).where(
					trailer_table.c.name == exist_title
				)
				result = connect.execute(stmt)
				trailer_df = self.sql2df(result)
				
				# review
				stmt = select(google_table).where(
					google_table.c.key == exist_title
				)
				result = connect.execute(stmt)
				google_df = self.sql2df(result)
				
				# anime
				stmt = select(anime_table).where(
					anime_table.c.name == exist_title
				)
				result = connect.execute(stmt)
				anime_df = self.sql2df(result)
				
			print(tmdb_df.shape, twitter_df.shape, spotify_df.shape, review_df.shape, trailer_df.shape, google_df.shape, anime_df)
		
		self.closeDB()
		
		return tmdb_df, twitter_df, spotify_df, review_df, trailer_df, google_df
				
	def user_choose(self, search_result: pd.DataFrame):
		# wait for implementation
		return search_result['title'][0]
	
	def sql2df(self, result):
		tmp = result.fetchall()
		
		if tmp == []:
			return None
		
		df = pd.DataFrame(tmp)
		df.columns = result.keys()
		
		return df
	
	def df2sql(self, df, tableName):
		with self.engine.connect() as connection:
		    df.to_sql(tableName, connection, if_exists = 'append', index = False)
		
	def linkDB(self):
		super().linkDB()
		
		metadata = MetaData()
		self.tm2_table = Table(
			self.table_name['tm2'], metadata, 
			autoload = True, autoload_with = self.engine
		)
		
		
		
		
		
		
