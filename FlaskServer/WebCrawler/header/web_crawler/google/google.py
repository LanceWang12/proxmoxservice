from serpapi import GoogleSearch
import pandas as pd 
import mysql.connector as connector
import mysql
from sqlalchemy import create_engine

def get_google_df(name):
  params = {
    "q": name,
    "api_key": "5c69756a330986888802983c64b4305fcbface92cc888bc281f610b1d6c1ad6f"
  }
  search = GoogleSearch(params)
  results = search.get_dict()
  # results
  knowledge_graph = results['knowledge_graph']
  # knowledge_graph
  search = knowledge_graph['people_also_search_for']
  #print(type(search))
  #print(len(search))

  recommend =[]
  img = []
  key = []
  link = []
  for i in search:
      key.append(name)
      #print(i['name'])
      recommend.append(i['name'])
      #print(i['image'])
      img.append(i['image'])
      #print(i['link'])
      link.append(i['link'])

  dict = {'key': key, 'recommend': recommend, 'img': img, 'link':link }  
  df = pd.DataFrame(dict) 
  #print(df)
  return df


# insert_google("駭客任務")
