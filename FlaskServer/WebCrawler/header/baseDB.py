import configparser
from sqlalchemy import create_engine, select, insert, MetaData, Table


class myDB():
	def __init__(self, config):
		self.config = config
		DBServer = 'DBServer'
		self.connect_url = f"mysql+pymysql://{self.config[DBServer]['DBUser']}:{self.config[DBServer]['DBPassword']}@{self.config[DBServer]['DBServerIp']}:3306/{self.config[DBServer]['DBName']}"
		
	def linkDB(self):
		try:
			self.engine = create_engine(self.connect_url)
		except Error as e:
			raise Warning("Create DB engine in failure...")
			
	def closeDB(self):
		try:
			self.engine.dispose()
		except Error as e:
			raise Warning("Close DB engin in failure...")
