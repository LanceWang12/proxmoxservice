import requests
import configparser

home_url = 'http://192.168.1.101:8888'

def testHomepage():
	r = requests.get(home_url)
	assert r.status_code == 200
	
def testRightLogin():
	url = home_url + '/login'
	
	
	
	# right login
	data = {
		'user_id': 'Lance12',
		'password': '12345'
	}
	r = requests.post(url, data = data)
	assert r.status_code == 200
	
	data = {
		'user_id': 'Nicole58',
		'password': '12345'
	}
	r = requests.post(url, data = data)
	assert r.status_code == 200
	
def testWrongLogin():
	url = home_url + '/login'
	# wrong login
	data = {
		'user_id': 'no this user',
		'password': '12345'
	}
	r = requests.post(url, data = data)
	assert r.status_code == 200
	
def testLogout():
	r = requests.get(home_url + '/logout')
	assert r.status_code == 200
	
def testRightSignup():
	url = home_url + '/signup'
	r = requests.get(url)
	assert r.status_code == 200
	
	# right login
	data = {
		'account': 'Lance',
		'password': '88888',
		'firstName': 'Lance',
		'lastName': 'Wang',
		'email': '1111@gmail.com'
	}
	r = requests.post(url, data = data)
	assert r.status_code == 200
	
def testWrongSignup():
	url = home_url + '/signup'
	r = requests.get(url)
	# wrong login
	data = {
		'account': 'Lance12',
		'password': '88888',
		'firstName': 'Lance',
		'lastName': 'Wang',
		'email': '1111@gmail.com'
	}
	r = requests.post(url, data = data)
	assert r.status_code == 200
	
def testWebCrawlerTwitter():
	url = home_url + '/WebCrawler' + '/twitter'
	r = requests.get(url)
	assert r.status_code == 200
	
def testWebCrawlertmdb():
	url = home_url + '/WebCrawler' + '/tmdb'
	r = requests.get(url)
	assert r.status_code == 200	
	
	
	
	
	
	
	
	
	
	
	
	
	
