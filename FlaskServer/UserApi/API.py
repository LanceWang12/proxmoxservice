from flask import Flask, render_template, request, url_for, redirect, flash, Blueprint
from flask_login import LoginManager, UserMixin, login_user, logout_user, login_required, current_user
import configparser

from header.myDB import myDB
from header.utils import getFeature

config = configparser.ConfigParser()
config.read('FlaskServer.ini')
DB = myDB(config)

userApp = Blueprint('userApi', __name__, template_folder = './template')

# -------- For account management --------
# set private key to flask app
userApp.secret_key = config['password']['privatekey']

class User(UserMixin):
    pass

login_manager = LoginManager()
login_manager.init_app(userApp)
login_manager.session_protection = "strong"
login_manager.login_view = 'login'
login_manager.login_message = 'please login'

tableName = 'userAccount'
cmpCol = 'account'
@login_manager.user_loader
def user_loader(userArg):
	result = DB.execute(f'select * from {tableName} where {cmpCol} = "{userArg}"')
	
	if result == None:
		return
	
	user = User()
	user.id = userArg
	# user.is_authenticated = (result != None)
	print(f'-------- {user.is_authenticated} --------')
	
	return user

@login_manager.request_loader
def request_loader(request):
	userArg = request.form.get('user_id')
	result = DB.execute(f'select * from {tableName} where {cmpCol} = "{userArg}"')
	
	if result == None:
		return

	user = User()
	user.id = userArg

	# user.is_authenticated = (result != None)

	return user

# -------- API definition --------
# 1. log in
# 2. log out
# 3. add movie in the cart(DB)
# 4. get the result of recommendation
# 5. crawler manager
#    ----| imdb
#    ----| twitter
#    ----| youtube

@userApp.route("/")
def home():
	return render_template('home.html')
	
@userApp.route('/signup', methods = ['GET', 'POST'])
def signup():
	if request.method == 'GET':
		return render_template("signup.html")
		
	account = request.form['account']
	result = DB.execute(f'select * from {tableName} where {cmpCol} = "{account}"')
    
	if result == None:
		columns = DB.execute(f"SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '{tableName}'")
		columns = getFeature(columns)
		
		password = request.form['password']
		firstName = request.form['firstName']
		lastName = request.form['lastName']
		email = request.form['email']
		
		sql = f"INSERT INTO {tableName} {columns} VALUES ('{account}', '{password}', '{firstName}', '{lastName}', '{email}')"
		DB.execute(sql, commit = True)
		return redirect(url_for("home"))
		
	flash('This username has been used, please use another one...')
	return render_template('signup.html')

@userApp.route('/login', methods = ['GET', 'POST'])
def login():
	if request.method == 'GET':
		return render_template("login.html")
        
	userArg = request.form['user_id']
	result = DB.execute(f'select * from {tableName} where {cmpCol} = "{userArg}"')
    
	if result != None:
		user = User()
		user.id = userArg
		login_user(user)
		flash(f'{userArg}! glad to join ICSDT team 9 !!!')
		return redirect(url_for("home"))
		
	flash('Login error! Please login again...')
	return render_template('login.html')

@userApp.route('/logout')
def logout():
	userArg = current_user.get_id()
	logout_user()
	flash(f'{userArg}！Welcome next time！')
	return render_template('login.html')
