# ICSDT - FlaseServer

## Architecture

```
FlaseServer ---- | wsgi.py
            ---- | baseApi.py
            ---- | UserApi
            ---- | WebCrawlerApi ---- | tmdb	---- | API.py
                                                ---- | template ---- | *.html, *.css, .js
                                 ---- | twitter	---- | API.py
                                                ---- | template ---- | *.html, *.css, .js
```

## How to run whole web service formally?

```bash
uwsgi --ini uWSGI_config.ini

# Then you can access http://192.168.1.101:8888/ in proxmox
# or access http://140.113.213.82:8888/ in the Internet
```

## How to cooperate with each other to design api?

1. Everyone can design their own api in their directory just like the above

2. How to link every api directory?

    ```python
    # -------- < API.py > --------
    from flask import Blueprint, render_template
    
    # template_folder: to tell flask where is your *.html, *.css, *.js
    exampleApp =  Blueprint('exampleApi', __name__, template_folder = './template')
    
    @exampleApp.route('/')
    def tmdb_flask():
      	# just an example code
        return render_template("index.html")
    ```

    ```python
    # -------- < baseApi.py > --------
    # You also need to register your api in baseApi.py
    
    from flask import Flask
    # !!!! please import your api !!!!
    from ??.??.API import exampleApp
    app = Flask(__name__)
    
    # register your api
    your_prefix = '/my_domain_name'
    app.register_blueprint(exampleApp, url_prefix = your_prefix)
    ```

3. How to run whole web application?

    ``` python
    # in directory: FlaskServer
    # There is a file: wsgi.py in FlaskServer
    # -------- The content in wsgi.py
    
    from baseApi import app
    
    # You can uncomment the below instruction to test on flask testing environment
    # app.run(debug = True)
    ```

    ```bash
    # in terminal
    # and then key in the bellow instruction to run whole web service
    python3 wsgi.py
    ```

