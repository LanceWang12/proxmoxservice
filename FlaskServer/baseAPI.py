from flask import Flask, render_template, request, url_for, redirect, flash, Blueprint, session
from flask_login import LoginManager, UserMixin, login_user, logout_user, login_required, current_user
import configparser

from header.userAccount import userTB, comparePWD

from header.web_crawler import web_crawler
	
app = Flask(__name__)
	
	
# register blureprint
# webcrawler api
#WebCrawlerPrefix = '/Web'
#app.register_blueprint(WebCrawlerApp, url_prefix = WebCrawlerPrefix)





config = configparser.ConfigParser()
config.read('FlaskServer.ini')
userInfo = userTB(config)

config.read('./header/web_crawler/Crawler.ini')
crawler = web_crawler(config)

# -------- For account management --------
# set private key to flask app
app.secret_key = config['password']['privatekey']

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.session_protection = "strong"
login_manager.login_view = 'login'
login_manager.login_message = 'please login'

class User(UserMixin):
    pass

@login_manager.user_loader
def load_user(account):
	
	userInfo.linkDB()
	pwd = userInfo.getUserPwd(account)
	userInfo.closeDB()
	
	if pwd == None:
		return
	
	curr_user = User()
	curr_user.id = account
	# user.is_authenticated = (result != None)
	# print(f'-------- {user.is_authenticated} --------')
	
	return curr_user


@login_manager.request_loader
def request_loader(request):
	account = request.form.get('email')
	userInfo.linkDB()
	pwd = userInfo.getUserPwd(account)
	userInfo.closeDB()
	print(f'------------------ {pwd} --------------------')
	if pwd != None:
		curr_user = User()
		curr_user.id = account
		curr_user.is_authenticated = True
		return curr_user
	else:
		return
	

# -------- API definition --------
# 1. log in
# 2. log out
# 3. add movie in the cart(DB)
# 4. get the result of recommendation
# 5. crawler manager
#    ----| imdb
#    ----| twitter
#    ----| youtube

@app.route("/")
def home():
	return render_template('main.html')
	
@app.route("/about")
def about():
	return render_template('about.html')

@app.route("/price")
def price():
	return render_template('price.html')

@app.route("/contact")
def contact():
	return render_template('contact.html')
	
@app.route("/service")
def service():
	return render_template('service.html')

@app.route('/signup', methods = ['GET', 'POST'])
def signup():
	if request.method == 'GET':
		return render_template("sign-up.html")
	
	
	# whether the username is in our table
	userInfo.linkDB()
	account = request.form['email']
	pwd = userInfo.getUserPwd(account)
    
	if pwd == None:
		username = request.form['username']
		password = request.form['password']
		firstName = request.form['first-name']
		lastName = request.form['last-name']
		email = request.form['email']
		
		userInfo.createAccount(account, password, firstName, lastName, email)
		
		userInfo.closeDB()
		
		return redirect(url_for("home", username = account.split('@')[0]))
		
	flash('This username has been used, please use another one...')
	return render_template('sign-up.html')

@app.route('/login', methods = ['GET', 'POST'])
def login():
	if request.method == 'GET':
		return render_template("login.html")
	
	userInfo.linkDB()
	account = request.form['email']
	userPwd = userInfo.getUserPwd(account)
	userInfo.closeDB()
	
	keyInPwd = request.form['password']
	
	if (userPwd != None) and comparePWD(userPwd, keyInPwd):
		curr_user = User()
		curr_user.id = account
		login_user(curr_user)
		flash(f'{account}! glad to join ICSDT team 9 !!!')
		return redirect(url_for("home", username = account.split('@')[0]))
		
	flash('Login error! Please login again...')
	return render_template('login.html')
	
@app.route('/logout')
@login_required
def logout():
	userArg = current_user.get_id()
	logout_user()
	session.clear()
	flash(f'{userArg}！Welcome next time！')
	return render_template('login.html')
	



@app.route('/web', methods = ['GET', 'POST'])
@login_required
def search():
	if current_user.is_authenticated:
		if request.method == 'GET':
			dict_lst = crawler.get_frontpage()
			return render_template('browsing.html', info = dict_lst)
		
		
		user_search = request.form['search']
		print(f'----------------- {user_search} ---------------------')
		
		dict_lst = crawler.search_flow(user_search)
		
		
		return render_template('search_result.html', info = dict_lst)
	else:
		render_template('main.html')
	

	
	
	
	
	
	
	
