import subprocess
import configparser

class FlaskServerConfig():
    def __init__(self):
        self.local_ip = subprocess.getoutput('hostname -I')[: -1]

        print("Please input the port number of the web service: ", end = '')
        self.WebServicePort = input()

        print("Please input the ip of DataBase: ", end = '')
        self.DBServerIp = input()

        print("Please input the user of DataBase: ", end = '')
        self.DBUser = input()

        print("Please input the password of this user: ", end = '')
        self.DBPassword = input()

        print("Please input the name of DataBase: ", end = '')
        self.DBName = input()

if __name__ == "__main__":
    # Let user key in some imformation
    FlaskConfig = FlaskServerConfig()
    
    # write user's imformation in config file
    config = configparser.ConfigParser()

    config['FlaskServer'] = {
            'local_ip': FlaskConfig.local_ip,
            'WebServicePort': FlaskConfig.WebServicePort
    }

    config['DBServer'] = {
        'DBServerIp': FlaskConfig.DBServerIp,
        'DBUser': FlaskConfig.DBUser,
        'DBPassword': FlaskConfig.DBPassword,
        'DBName': FlaskConfig.DBName
    }
    
    config['password'] = {
    	'privatekey' : 'hdfjkcn9werqjm,axpwxueyjcfv duo'
    }

    with open('FlaskServer.ini', 'w') as configfile:
        config.write(configfile)



    # read uwsgi config file
    config = configparser.ConfigParser()
    config.read('uWSGI_config.ini')
    config['uwsgi']['http'] = f'{FlaskConfig.local_ip}:{FlaskConfig.WebServicePort}'
    with open('uWSGI_config.ini', 'w') as configfile:
        config.write(configfile)
