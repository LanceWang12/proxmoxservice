#!/bin/bash

NginxConfigPath="/etc/nginx"

sudo cp ./flask.conf $NginxConfigPath/sites-available/flask.conf
sudo rm $NginxConfigPath/sites-enabled/default

cd $NginxConfigPath/sites-enabled
sudo ln -s ../sites-available/flask.conf

sudo systemctl restart nginx.service