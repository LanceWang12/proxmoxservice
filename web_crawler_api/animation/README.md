# animation

| Column Name | Datatype    | PK   | Remarks  |
| ----------- | ----------- | :--- | -------- |
| name        | VARCHAR(60) | yes  | name     |
| img         | VARCHAR(80) |      |          |
| view        | VARCHAR(20) |      | 觀看數   |
| rank        | VARCHAR(20) |      | 總分 5分 |
| type        | VARCHAR(50) |      |          |

