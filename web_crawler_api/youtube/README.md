# youtube-trailer

### Remarks prefix: https://www.youtube.com/embed/

| Column Name | Datatype    | PK   | Remarks         |      |
| ----------- | ----------- | :--- | --------------- | ---- |
| name        | VARCHAR(45) | V    | movie_name      |      |
| trailer1    | VARCHAR(45) |      | top1 trailer ID |      |
| trailer2    | VARCHAR(45) |      | top2 trailer ID |      |
| trailer3    | VARCHAR(45) |      | top3 trailer ID |      |
| trailer4    | VARCHAR(45) |      | top4 trailer ID |      |
| trailer5    | VARCHAR(45) |      | top5 trailer ID |      |

# youtube-review
| Column Name | Datatype    | PK   | Remarks        |
| ----------- | ----------- | ---- | -------------- |
| name        | VARCHAR(45) | V    | movie_name     |
| review1     | VARCHAR(45) |      | top1 review ID |
| review2     | VARCHAR(45) |      | top2 review ID |
| review3     | VARCHAR(45) |      | top3 review ID |
| review4     | VARCHAR(45) |      | top4 review ID |
| review5     | VARCHAR(45) |      | top5 review ID |
| review6     | VARCHAR(45) |      | top6 review ID |
| review7     | VARCHAR(45) |      | top7 review ID |
| review8     | VARCHAR(45) |      | top8 review ID |
| review9     | VARCHAR(45) |      | top9 review ID |
| review10    | VARCHAR(45) |      | top8 review ID |

