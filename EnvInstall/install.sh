#!/bin/bash

sudo apt-get update
sudo apt-get -y dist-upgrade
sudo apt-get clean
sudo apt-get autoremove


sudo apt install python3.8
sudo apt install python3-pip
sudo pip3 install -r requirement.txt

sudo apt-get install nginx
sudo apt install mariadb-server
sudo apt install mariadb-client
sudo apt install vim
sudo apt install git
sudo apt install curl
sudo apt install docker

wget https://github.com/mozilla/geckodriver/releases/download/v0.30.0/geckodriver-v0.30.0-linux64.tar.gz
tar -xvzf geckodriver*
chmod +x geckodriver
sudo mv geckodriver /usr/local/bin/
rm geckodriver*
