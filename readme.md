# ICSDT - Movie intergration platform

# - System Architecture

<img src="./some_img/arch.png" alt="system" style="zoom:150%;" />

1. We select mariaDB as the database because our data is associative.
2. Select Flask as the website framework, since its community is complete and powerful.
3. In future, api-gateway may link to database server directly to speed up the access speed.

# - Specification

1. Client will login to the web

2. Client can pick their favorite movies

3. Our service will display some information:
   -  The music in the movies
   - Some comment and discussion about the movie
   
4. Database description(just a table like dataframe)

   ```mysql
   userAccount(
   	id INT not null AUTO_INCREMENT,
     	account varchar(100) not null,
     	password varchar(100) not null,
     	firstName varchar(100) not null,
     	lastName varchar(100) not null,
     	email varchar(100) not null,
     	primary key (id)
   );
   ```

   ![database](./some_img/database.png)

# - API design (Restful)

1. /
   - GET: go to home page
2. /login
   - GET: go to login page
   - POST: login to our service
3. /logout
   - GET: logout
4. /music
   - GET: go to the page about music
   - POST: request for some music
5. /review
   - GET: go to the page about review
   - POST: request for some information about review

# - Infrastructure

1. Install and update some package:

   ``` bash
   # First, please install and update some package in VM. Second, user can clone this base VM, 
   # and then follows bellow instructions to build NginxServer, FlaskServer, and DBServer.
   cd ./EnvInstall
   
   chmod 777 install.sh
   ./install.sh
   ```

   

2. NGINX Server :

   ```bash
   cd ./NginxServer
   
   chmod 777 initialize.sh
   ./initialize.sh
   ```

3. Flask Server :
   ``` bash
   cd ./FlaskServer
   
   # User can type some config by keyboard
   python3 initialize.py
   
   # User can also use default config
   python3 initialize.py < DefaultConfig
   
   # Start uwsgi service
   uwsgi --ini uWSGI_config.ini
   ```

4. MariaDB Server:
   ```bash
   cd ./DBServer
   
   chmod 777 initialize.sh
   
   # if you first set up the Database, please add first tag
   ./initialize.sh --first
   ```

5. pfSense:
   ```markdown
   Create two linux bridge from proxmox.
     you can create bridge by GUI settings or '/etc/network/interfaces'.
     vmbr1 for WLAN, vmbr2 for LAN.
   
   Reboot proxmox, check whether these two bridges are active.
     vmbr1 and vmbr2
   
   Create new VM with your pfSense iso file.
     choose vmbr1 as bridge from GUI settings.
   
   Add new Network Device to pfSense->Hardware.
     choose vmbr2 as bridge from GUI settings.
     we will have two Network Device at present.
   
   Start your pfSense machine, set WLAN to vmbr1 and LAN to vmbr2.
      we can ignore VLAN.
   
   Enjoy it.
   ```

