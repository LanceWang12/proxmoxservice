#!/bin/bash

start DB service
sudo systemctl start mariadb
sudo systemctl enable mariadb
sudo python3 SetSqlConfig.py

if [ $# -ge 1 ]
then
    if [ $1 == "-first" ]
    then
        sudo mysql_secure_installation
        sudo mysql –u root –p < ./InitialDB.sql
    else
        echo I don\'t know what $1 mean...
        exit 1
    fi
else
    sudo mysql –u root –p < ./InitialDB.sql
fi

sudo systemctl start mariadb
sudo ufw allow 3306/tcp