# DBServer
## How to use mysql workbench

1. Open mysql-workbench by your terminal

```bash
# key in the following instuction
mysql-workbench
# then mysqlworkbench will be opened
```

2. Add connection to your account
    ![](https://i.imgur.com/MuFWzVa.png)

3. Edit your account information

    ![](https://i.imgur.com/HdAHBA2.png)

4. Add "useSSL=0"
    ![](https://i.imgur.com/LNy37s8.png)

5. And then you can connect to DBServer by your account
    ![](https://i.imgur.com/FTSrBb7.png)

## Easy tutorial to SqlAlchemy

The start of any SQLAlchemy application is an object called the Engine. The engine is typically a global object created just once for a particular database server, and is configured using a **URL string** to connect with a database .  If you want to see detailed tutorial, you can see the following [url](https://docs.sqlalchemy.org/en/14/tutorial/engine.html).

```python
# construct an engine
connect_url = 'mariadb+mariadbconnector://username:password@ip:port_num/databasename'
engine = create_engine(connect_url)

# get sql table by python object
metadata = MetaData()
table = Table(tableName, metadata, autoload = True, autoload_with = self.engine)

# start one connection to implement some sql instruction
with engine.connect() as connect:
	stmt = select(table.c.password).where(table.c.account == userName)
	result = connect.execute(stmt)

# close the engine
# ---- You must close the engine please!! ----
engine.dispose()
```

## Json to SQL

Please read the following [url](https://blog.jcharistech.com/2020/01/08/how-to-convert-json-to-sql-format-in-python/)

## The information about your account

|        |       Account       | Code |
| :----: | :-----------------: | :--: |
|  張敬  | Chang@192.168.1.109 | 1234 |
| 湯智惟 | Tang@192.168.1.108  | 1234 |





## Don't see any code in this directory

