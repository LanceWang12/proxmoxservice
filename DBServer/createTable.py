import configparser
    
import mysql.connector as connector
import mysql
from sqlalchemy import create_engine
import pandas as pd


df = pd.read_csv('./logInData.csv')

engine = create_engine('mysql+mysqlconnector://lance:1234@localhost/users')
connection = engine.connect()

tableName = 'userAccount'

try:
	frame = df.to_sql(tableName, connection, if_exists='append', index=False)
except ValueError as vx:
    print(vx)
except Exception as ex:   
    print(ex)

else:
    print(f"Table [{tableName}] created successfully.");   

finally:
    connection.close()
